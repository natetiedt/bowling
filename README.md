# README #

### What is this repository for? ###

for sharing the code!

### How do I get set up? ###

	First, make sure you have the java environment downloaded from https://java.com/en/download/
	This project should be compatible with java 1.7

	git clone this repo
	go to the command line
	change directories to the main project folder
	run: 'java -jar Bowl.jar'
	
	
### TroubleShooting ###
	I don't think you'll need to, but you might need to run chmod 755 Bowl.jar on the root directory for the project