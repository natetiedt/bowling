/**
 * an object for on frame of the bowling game
 * 
 * @author tiedtn58
 *
 */
public class Frame {
	private int[] score = {0,0,0};
	private int extraPoints = 0;
	private int bowlsToAdd = 0;
	private int bowlsThisFrame = 0;
	private boolean bowledStrike = false,
			bowledSpare = false,
			isTenthFrame = false;

	/**
	 * constructor that checks if the frame is the tenth frame or not
	 * @param numberOfFrame
	 */
	public Frame(int numberOfFrame) {
	    if (numberOfFrame == 10) {
	    	isTenthFrame = true;
	    }
	}

	/**
	 * for grabbing the total score for the frame
	 * @return {number} - the total score including extra points from strikes and spares
	 */
	public int getFrameTotal() {
	    int sum = 0;
	    for(int i : score)
	      sum += i;
	    return sum + extraPoints;
	}
	
	/**
	 * used as a check for whether a player is entering a valid number of pins knocked down
	 * @return {number} - only the total number of pins knocked down
	 */
	public int getTotalPinsKnockedDown() {
		if(isTenthFrame && bowledStrikeOrSpare())
			return -10;
		int sum = 0;
	    for(int i : score)
	      sum += i;
	    return sum;
	}

	/**
	 * @return {boolean} - whether a strike or spare has been bowled
	 */
	public boolean bowledStrikeOrSpare() {
		return bowledStrike || bowledSpare;
	}
	
	/**
	 * @return {boolean} - whether a spare has been bowled or not
	 */
	public boolean bowledStrike() {
		return bowledStrike;
	}
	
	/**
	 * method to add extra points from a strike or spare being bowled in this frame
	 * @param {number} points - points to be added 
	 */
	public void addExtraPoints(int points) {
		if (bowlsToAdd > 0) {
			extraPoints += points;
			bowlsToAdd--;
		}
	}
	
	/**
	 * whether this frame bowled a strike or spare and is still waiting to add points for those
	 * @return {number} - number of times to add points
	 */
	public int bowlsToAddRemaining() {
		return bowlsToAdd;
	}
	
	/**
	 * adds points from a single bowl
	 * @param {number} pinsKnockedDown - score for one bowl
	 */
	public void setBowl(int pinsKnockedDown) {
		if(isTenthFrame && bowlsToAdd > 0) {
			extraPoints += pinsKnockedDown;
		}
		if(!checkForStrike(pinsKnockedDown)) {
			checkForSpare(pinsKnockedDown);
		}
		
		if (!isTenthFrame && bowlsThisFrame < 2) {
			score[bowlsThisFrame++] = pinsKnockedDown;
		} else if (isTenthFrame && bowlsThisFrame < 3) {
			score[bowlsThisFrame++] = pinsKnockedDown;
		}
		
	}
	
	/**
	 * checks if a strike was bowled based on the number of pins knocked down and which bowl it was
	 * @param {number} pinsKnockedDown 
	 * @return {boolean} 
	 */
	public boolean checkForStrike(int pinsKnockedDown) {
		if(!isTenthFrame && pinsKnockedDown == 10 && bowlsThisFrame == 0) {
			bowledStrike = true;
			if(!isTenthFrame) {
				bowlsThisFrame = 1;
			}
			bowlsToAdd = 2;
			return true;
		} else if (isTenthFrame && pinsKnockedDown == 10) {
			bowledStrike = true;
			bowlsToAdd = 2;
			return true;
		}
		return false;
	}
	
	/**
	 * checks if a spare has been bowled
	 * @param {number} pinsKnockedDown
	 * @return {boolean}
	 */
	public boolean checkForSpare(int pinsKnockedDown) {
		if(pinsKnockedDown + score[0] == 10 && bowlsThisFrame == 1) {
			bowledSpare = true;
			bowlsToAdd = 1;
			return true;
		}
		return false;
	}
	
	/**
	 * a check for whether this frame is done or not
	 * @return {number} - number of bowls remaining in this frame
	 */
	public int bowlsRemaining() {
		return isTenthFrame && bowledStrikeOrSpare() ? (3 - bowlsThisFrame) : (2 - bowlsThisFrame);
	}
	
	/**
	 * @return {number} - number of bowls for this frame so far
	 */
	public int currentBowl() {
		return bowlsThisFrame + 1;
	}
	  
}
