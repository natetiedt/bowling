import java.util.ArrayList;
import java.util.Scanner;

/**
 * main class for the bowling game
 * @author tiedtn58
 *
 */
public class Bowl {
	static ArrayList<String> users = new ArrayList<String>();
	static ArrayList<Game> games = new ArrayList<Game>();
	static Scanner scanner = new Scanner(System.in);
	
	public static void main(String[] args) {

		setUsers(scanner);
		
		setGames();
		
		playGames(scanner);
		
		scanner.close();
		
		for(int j = 0, numOfGames = games.size(); j < numOfGames; j++) {
			System.out.println("Score was this: " + games.get(j).getScore());
			System.out.println("Good Job");
		}
		
	}
	
	public static void setUsers(Scanner scanner) {
		System.out.println("You can play with up to 2 friends!");
		boolean next = true;
		do {
			System.out.println("What is a user's name?");
			while(!scanner.hasNext()) {
				System.out.println("Please enter a valid name.");
				scanner.next();
			}
			String input = scanner.nextLine();
			if (users.size() > 0 && input == "stop") {
				next = false;
			} else if (users.size() == 2) {
				users.add(input);
				next = false;
			} else {
				users.add(input);
			}
		} while(next != false);
	}
	
	public static void setGames () {
		for(int i = 0, size = users.size(); i < size; i++) {
			Game tempGame = new Game(users.get(i));
			games.add(tempGame);
		}
	}
	
	public static void playGames(Scanner scanner) {
		for(int i = 0; i < 10; i++) {
			for(int j = 0, numOfGames = games.size(); j < numOfGames; j++) {
				games.get(j).bowlFrame(scanner);
			}
		}
	}

}
