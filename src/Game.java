import java.util.ArrayList;
import java.util.Scanner;

/**
 * game object that contains frames
 * @author tiedtn58
 *
 */
public class Game {
	private ArrayList<Frame> frames = new ArrayList<Frame>();
	String userName = "";
	
	public Game(String name) {
		userName = name;
	}
	
	/**
	 * grabs all of the bowls a user might have for a single frame
	 * @param scanner - the scanner object since we can't close it until the app is complete
	 */
	public void bowlFrame(Scanner scanner) {
		Frame temp = new Frame(frames.size() + 1);
			
		do{
			System.out.println(userName + ", please enter the number of pins knocked down with bowl number " + temp.currentBowl() + " in the " + formatCardinal(frames.size() + 1) + ".");
			while(!scanner.hasNextInt()) {
				System.out.println("That's not a valid number of pins.");
				scanner.next();
			}
			int pinsKnockedDown = scanner.nextInt();
			if(pinsKnockedDown + temp.getTotalPinsKnockedDown() <= 10) {
				temp.setBowl(pinsKnockedDown);
				for(int i = 2; i > 0; i--) {
					int index = frames.size() - i;
					if(index > 0) {
						if(frames.get(index).bowlsToAddRemaining() > 0) {
							frames.get(index).addExtraPoints(pinsKnockedDown);
						}
					}
				}
			} else {
				System.out.println("That's not a valid number of pins.");
			}
		} while (temp.bowlsRemaining() > 0);
		frames.add(temp);
		System.out.println("Current score for " + userName + ": " + getScore());
	}
	
	/**
	 * totals the score for the game at the end of any frame
	 * @return
	 */
	public int getScore() {
		int score = 0;
		for(Frame f : frames) {
			score += f.getFrameTotal();
		}
		return score;
	}
	
	public String formatCardinal(int number) {
		String string = "";
		switch(number) {
		case 1:
			string = "1st";
			break;
		case 2:
			string = "2nd";
			break;
		case 3:
			string = "3rd";
			break;
		default:
			string = Integer.toString(number) + "th";
		}
		return string;
	}
}
